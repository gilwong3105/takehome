import React from 'react';

const OddTiles = ({ cells, even, odd }) => {
  return (
    <div className='container'>
      {cells.map((cell, i) => {
        return i % 2 === 0 ? (
          <div key={i} style={{ ...odd }}></div>
        ) : (
          <div key={i} style={{ ...even }}></div>
        );
      })}
    </div>
  );
};

export default OddTiles;
