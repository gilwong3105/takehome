import React from 'react';
import EvenTiles from './EvenTiles';
import OddTiles from './OddTiles';

const even = {
  display: 'inline-block',
  backgroundColor: 'red',
  width: 50,
  height: 50,
  border: '1px solid black',
};

const odd = {
  display: 'inline-block',
  backgroundColor: 'blue',
  width: 50,
  height: 50,
  border: '1px solid black',
};

const Board = ({ columns }) => {
  const dimensions = new Array(parseInt(columns)).fill();

  return (
    <div>
      {dimensions.map((val, i) =>
        i % 2 === 0 ? (
          <EvenTiles key={i} cells={dimensions} even={even} odd={odd} />
        ) : (
          <OddTiles key={i} cells={dimensions} even={even} odd={odd} />
        )
      )}
    </div>
  );
};

export default Board;
