import React, { useState } from 'react';

const Input = ({ handleSubmit }) => {
  const [columns, setColumns] = useState(0);

  return (
    <div>
      <input
        placeholder='Enter a number'
        onChange={(e) => setColumns(e.target.value)}
      />
      <button type='button' onClick={() => handleSubmit(columns)}>
        Create Board
      </button>
    </div>
  );
};

export default Input;
