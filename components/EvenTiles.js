import React from 'react';

const EvenTiles = ({ cells, even, odd }) => {
  return (
    <div className='container'>
      {cells.map((cell, i) => {
        return i % 2 === 0 ? (
          <div key={i} style={{ ...even }}></div>
        ) : (
          <div key={i} style={{ ...odd }}></div>
        );
      })}
    </div>
  );
};

export default EvenTiles;
