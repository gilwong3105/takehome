import { useState } from 'react';
import Input from '../components/Input';
import Board from '../components/Board';

export default () => {
  const [dimensions, setDimensions] = useState(0);

  const handleSubmit = (value) => {
    if (Number.isInteger(parseInt(value))) {
      setDimensions(value);
    } else {
      alert('Value must be a number');
    }
  };

  return (
    <div className='main'>
      <Input handleSubmit={handleSubmit} />

      {dimensions > 0 && (
        <div style={{ paddingTop: 20 }}>
          <Board columns={dimensions} />
        </div>
      )}
      <style jsx>{`
        .main {
          margin: 0 auto;
          padding: 10px;
        }
      `}</style>
    </div>
  );
};
